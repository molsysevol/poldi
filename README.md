# Poldi

Approximate Bayesian Computation (ABC) to estimate the fitness effect of the _Pldi_ de novo gene.

The Simulations directory contains the R code to simulate data under the specified demography.
The EstimateParameters notebook describes the ABC analyses per se: model fitting, parameter estimation, cross-validation.

We first try several models with random mating, using different fitness parameterization:

* Sex-averaged: one h and one s for all individuals
* Sex-specific: hf and sf for females, hm ans sm for males
* Males only: hf and sf = 0, hm and sm estimated

In both cases, we fitted a model one the allele frequencies only, and one on the genotype frequencies.
We then fit several models with non random mating, using the "males only" fitness parameterization:

*


